package com.company.booleans;

public class AllCaps {

    public static void main(String[] args) {
        String shout = "HELLO";
        String say = "Hey";

        System.out.println(allCaps(shout)); // true
        System.out.println(allCaps(say)); // false
    }

    public static boolean allCaps(String shout) {
        char[] chars = shout.toCharArray();
        // Iterate over a string
        // For each character
        for (int i = 0; i < chars.length; i++) {
            // if is upper case letter
            boolean isCharUppercase = Character.isUpperCase(chars[i]);
            if (!isCharUppercase) {
                return false;
            }
            // update result
        }
        return true;
    }
}
