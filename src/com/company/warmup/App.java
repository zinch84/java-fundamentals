package com.company.warmup;

public class App {
    public static void main(String[] args) {
        int currentYear = 2020;
        System.out.println(currentYear);

        {
            int temp = 1000;
            System.out.println(currentYear + temp);
        }

        for (int i = 0, j = 100; i < 2; ++i) {
            System.out.println(i + j);
        }

        for (int i = 0; i < 2; ++i) {
            System.out.println(i);
        }
    }
}
