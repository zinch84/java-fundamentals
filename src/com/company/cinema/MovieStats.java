package com.company.cinema;

public class MovieStats {

    public static double ratingTotal() {
        double sum = 0;
        for (Movie movie : MovieDatabase.movies) {
            sum += movie.getRating();
        }
        return sum;
    }

    public static void printAverageRating() {
        double total = ratingTotal();
        int count = MovieDatabase.movies.length;

        double avg = total / count;

        System.out.println("Average is " + avg);
    }

    public static void printMoviesWithNameLessThan10AndRatingMoreThan8() {
        for (Movie movie : MovieDatabase.movies) {
            if (movie.getName().length() < 10 && movie.getRating() >= 8) {
                System.out.println(movie);
            }
        }
    }

    public static void diffBetweenHighestAndLowestRating() {
        Movie highestRating = MovieDatabase.movies[0];
        Movie lowestRating = MovieDatabase.movies[0];

        for (Movie movie : MovieDatabase.movies) {
            if (movie.getRating() > highestRating.getRating()) {
                highestRating = movie;
            }

           if (movie.getRating() < lowestRating.getRating()) {
               lowestRating = movie;
           }
        }

        double diff = highestRating.getRating() - lowestRating.getRating();
        System.out.println("Diff between highest rating and the lowest is " + diff);
    }


    public static void main(String[] args) {
        //printMoviesWithNameLessThan10AndRatingMoreThan8();
        //printAverageRating();
        diffBetweenHighestAndLowestRating();
    }
}
