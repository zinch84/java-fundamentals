package com.company.general;

public class StringDemo {
    public static void main(String[] args) {
        java.lang.String x = "AbC";
        java.lang.String y ="abC";

        System.out.println(x.equals(y));

        x.toUpperCase(); // does not work!!!

        x = x.toUpperCase();
        y = y.toUpperCase();
        System.out.println(x);
        System.out.println(y);

        System.out.println(x == y); // do not use!
        System.out.println(x.equals(y));

        System.out.println(x.contains(y));
    }
}
