package com.company.strings;

public class Strings {
    public static void main(String[] args) {
        String[] names = {"alice", "ALICE", "AlIcE", "", null, "bob"};

        for (String name: findNonEmpty(names)) {
            System.out.println(capitalize(name));
        }
    }

    // [] -> []
    // [null] -> []
    // [""] -> []
    // [Alice, null, bob] -> [Alice, bob]
    public static String[] findNonEmpty(String[] input) {
        if (input == null) {
            return new String[0];
        }

        if (input.length == 0) {
            return input;
        }

        int elementsToKeepCount = 0;

        // Decide how many elements to keep from original array
        // For each element
        for (String s: input) {
            // Check if it is not (null or empty)
            boolean isNullOrEmpty = (s == null) || s.isEmpty();
            boolean isValidString = !isNullOrEmpty;

            if (isValidString) {
                elementsToKeepCount += 1;
            }
        }

        System.out.println("Found " + elementsToKeepCount + " valid string");

        // Create new array
        String[] result = new String[elementsToKeepCount];

        // Populate result
        int insertIndex = 0;

        // The same loop again
        for (String s: input) {
            // Check if it is not (null or empty)
            boolean isNullOrEmpty = (s == null) || s.isEmpty();
            boolean isValidString = !isNullOrEmpty;

            if (isValidString) {
                // Put string in result at index insertIndex
                result[insertIndex] = s;
                // Increment the insertIndex
                insertIndex += 1;
            }
        }

        // return result
        return result;
    }

    public static String capitalize(String name) {
        // Check for null -> return an empty string
        if (name == null) {
            return "";
        }

        // Check for empty string -> return an empty string
        if (name.isEmpty()) {
            return "";
        }

        String firstLetter = name.substring(0, 1);
        firstLetter = firstLetter.toUpperCase();

        String rest = name.substring(1);
        rest = rest.toLowerCase();
        return firstLetter + rest;
    }
}
