package com.company.general;

public class Names {

    public static void main(String[] args) {
        String x = "Hello";
        print(x);
        String y = "WOrld";
        print(y);

        class Foo {
            public String say() {
                return "Hello";
            }
        }

        Foo foo = new Foo();

    }

    public static void print(String anyName) {
        System.out.println(anyName);

        countChars(anyName);
    }

    public static int countChars(String input) {
        return input.length();
    }
}
