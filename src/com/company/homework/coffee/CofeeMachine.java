package com.company.homework.coffee;

class CoffeeMachine {

    public static enum CoffeeType { ESPRESSO, AMERICANO, LATTE, FLAT_WHITE }

    public static void main(String... args) {
        CoffeeMachine coffeeMachine = new CoffeeMachine(11, 7.5, 100);
        coffeeMachine.brewCoffee(CoffeeType.FLAT_WHITE);
    }

    private double milk;
    private double water;
    private int beans;

    public CoffeeMachine(double milk, double water, int beans) {
        this.milk = milk;
        this.water = water;
        this.beans = beans;
    }

    public void brewCoffee(CoffeeType coffeeType) {

        // Espresso requires
        // 1.25 water
        // 11 beans

        // Americano requires
        // 1.95 water
        // 9 beans

        // Flat White requires
        // 1.4 water
        // 0.45 milk
        // 10 beans

        // Latte requires
        // 1.1 water
        // 0.75 milk
        // 10 beans

        // If cannot make coffee
        // print "Cannot make <coffee type>, not enough beans",
        // where coffee type is a ENUM name
        // otherwise
        // print "<coffee type is ready>"
    }

    public void printState() {
        // if no beans prints - no beans
        // if no milk prints - no milk
        // if no water prints - no water

        // should print only one message
        // if no beans and no water -> "No beans, No water"
    }
}
