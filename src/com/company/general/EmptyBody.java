package com.company.general;

public class EmptyBody {
    public static void main(String[] args) {
        if (false)
            //;
        {
            System.out.println("Should never happen!!!");
        }

        boolean flag = true;

        while (flag)
        //;
        {
            System.out.println("I'm running!");
            flag = false;
        }
    }
}
