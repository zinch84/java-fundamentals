package com.company;

import java.util.Objects;

public class Point {

    private int x;
    private int y;

    // Alt + Insert
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    // Shift + Shift
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;

        return x == point.x && y == point.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    public static void main(String[] args) {
        Point zero = new Point(0,0);
        Point anotherPoint = new Point(0, 0);

        System.out.println(1 == 1);
        System.out.println(zero.equals(anotherPoint));

        Point x = new Point(1, 2);
        Point y = new Point(2, 1);
        System.out.println(zero.equals(x));
    }
}
