package com.company.cinema;

public class TicketOffice {

    private int ticketsSold;

    public void sellTicket(int count) {
        if (count < 0) {
            System.out.println("Wrong input");
        } else {
            ticketsSold += count;
        }
    }

    private void close() {
        ticketsSold = 0;
    }

    @Override
    public String toString() {
        return "TicketOffice{" +
                "ticketsSold=" + ticketsSold +
                '}';
    }

    public static void main(String[] args) {
        TicketOffice office = new TicketOffice();
        office.sellTicket(10);
        System.out.println(office);
        office.close();
        office.sellTicket(-100);
        System.out.println(office);
    }
}
