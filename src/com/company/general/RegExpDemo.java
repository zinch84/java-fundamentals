package com.company.general;

import java.util.regex.Pattern;

public class RegExpDemo {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("Hello[\\s]+World!");

        String a = "Hello World!";
        String b = "Hello    World!";

        System.out.println(pattern.matcher(a).matches());
        System.out.println(pattern.matcher(b).matches());

        Pattern allDigits = Pattern.compile("[\\d]+");

        System.out.println(allDigits.matcher("123123123123").matches());
        System.out.println(allDigits.matcher("12AAAA").matches());

        Pattern anyCharacterPattern = Pattern.compile(".");

        System.out.println(anyCharacterPattern.matcher("A").matches());
        System.out.println(anyCharacterPattern.matcher("1").matches());

        Pattern literalDot = Pattern.compile("\\.");
        System.out.println(literalDot.matcher("A").matches());
        System.out.println(literalDot.matcher(".").matches());
    }
}
