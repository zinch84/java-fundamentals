package com.company.cinema;

public class Range {

    private final int start;
    private final int end;

    public Range(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public boolean containsInclusive(int number) {
        return number >= start && number <= end;
    }
}
