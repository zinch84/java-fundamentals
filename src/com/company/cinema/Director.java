package com.company.cinema;

import static com.company.cinema.Movie.Genre.ACTION;

public class Director extends Human {

    private String name;
    private Movie.Genre genre;

    public Director(String name, Movie.Genre genre) {
        this.name = name;
        this.genre = genre;
    }

    public Movie makeMovie(String title, int year) {
        return new Movie(title, year, 0, genre);
    }

    public static void main(String[] args) {
        Director director = new Director("Guy Richie", ACTION);
        System.out.println(director);
        director.sayHello();
        System.out.println(director.makeMovie("Snatch", 2010));
        System.out.println(director.makeMovie("Lock Stock & 2 barrels", 2015));
    }

    @Override
    public String toString() {
        return "Director{name='" + name + '\'' + '}';
    }
}
