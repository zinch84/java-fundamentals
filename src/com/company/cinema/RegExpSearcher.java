package com.company.cinema;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExpSearcher {

    public static void main(String[] args) {
        // \w 	A word character: [a-zA-Z_0-9]
        Pattern singleWord = Pattern.compile("\\w+");

        Pattern twoWords = Pattern.compile("\\w+[ ]+\\w+");

        Pattern hasDigit = Pattern.compile(".*[0-9]+");

//        Greedy quantifiers
//        X? 	X, once or not at all
//        X* 	X, zero or more times
//        X+ 	X, one or more times

        Pattern startsWithS = Pattern.compile("S.*");
        System.out.println(Pattern.compile("S.+").matcher("S").matches());
        searchMoviesMatchingRegExp(startsWithS);
    }

    public static void searchMoviesMatchingRegExp(Pattern pattern) {
        for (Movie movie: MovieDatabase.movies) {
            String title = movie.getName();

            Matcher matcher = pattern.matcher(title);

            if (matcher.matches()) {
                System.out.println(movie);
            }
        }
    }
}
