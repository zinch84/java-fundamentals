package com.company.cinema;

public class Actor extends Human {

    // Alt + Insert
    @Override
    public void sayHello() {
        System.out.println("I'm an actor!");
    }

    public static void main(String[] args) {
        Actor actor = new Actor();
        System.out.println(actor);

        actor.sayHello();
    }
}
