package com.company.booleans;

public class AllNumbers {
    public static void main(String[] args) {
        String numeric = "123123";
        String id = "x122";
        String greeting = "Hello";

        System.out.println("All numbers");
        System.out.println(allNumbers(numeric));
        System.out.println(allNumbers(id));
        System.out.println(allNumbers(greeting));
        System.out.println("Any numbers");
        System.out.println(anyNumbers(numeric));
        System.out.println(anyNumbers(id));
        System.out.println(anyNumbers(greeting));
    }

    private static boolean anyNumbers(String input) {
        for (char c: input.toCharArray()) {
            if (Character.isDigit(c)) {
                return true;
            }
        }
        return false;
    }
    
    private static boolean allNumbers(String input) {
        char[] chars = input.toCharArray();
        // Iterate over a string
        // For each character
        for (int i = 0; i < chars.length; i++) {
            // if is upper case letter
            if (!Character.isDigit(chars[i])) {
                return false;
            }
        }
        return true;
    }
}
