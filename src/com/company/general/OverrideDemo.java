package com.company.general;

import java.util.Objects;

public class OverrideDemo {

    static class MyClass {
        private final String name;

        MyClass(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            MyClass myClass = (MyClass) o;
            return Objects.equals(name, myClass.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name);
        }
    }

    public static void main(String[] args) {
        MyClass foo = new MyClass("foo");
        MyClass bar = new MyClass("bar");

        System.out.println(foo.equals("foo"));
        System.out.println(foo.equals(foo));

        System.out.println(foo.equals(bar));
        System.out.println(foo.equals(new MyClass("foo")));
    }
}
