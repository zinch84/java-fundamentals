package com.company.general;

import com.company.cinema.Movie;

import static com.company.cinema.Movie.Genre.*;

public class Aliasing {
    public static void main(String[] args) {
        Movie movie = new Movie("Test", 1000, 0, ACTION, DRAMA);

        Movie.Genre[] genres = movie.getGenres();
        genres[0] = COMEDY;
        genres[1] = FANTASY;

        System.out.println(movie);
    }
}
