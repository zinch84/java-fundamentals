package com.company.general;

public class CommonErrors {

    private static int age = 100;

    public static void main(String[] args) {
        CommonErrors obj = new CommonErrors();
        obj.sayHi();
    }

    public static String getName() {
        String name = "Bob";
        return name;
    }

    static int getAge() {
        return age;
    }

    private double getAverageTemp() {
        return 3.5;
    }

    public void sayHi() {
        System.out.println("Hi");
    }
}
