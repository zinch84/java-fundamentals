package com.company.homework.parking;

import java.time.DayOfWeek;

class Parking {

    public static void main(String... args) {
        Parking parking = new Parking();
        // example input
        //
        System.out.println("Two hours on monday for normal car cost : " +
                parking.calculatePrice(2, DayOfWeek.MONDAY, false));
    }

    public double calculatePrice(int hours, DayOfWeek day, boolean isBigCar) {

        // for normal cars
        //
        // monday - friday
        // first hour is free
        // each next hour costs 0.75
        // Saturday
        // first two hours are free
        // each next hour costs 1.10
        // free on Sunday

        // for big car
        // monday - friday
        // each hour costs 1.25
        // Saturday
        // each hour costs 1.90
        // free on Sunday

        return 0;
    }
}