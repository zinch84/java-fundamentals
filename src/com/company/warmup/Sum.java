package com.company.warmup;

public class Sum {
    public static void main(String[] args) {
        sumNumberThatAreOdd();
        sumNumbersFromArrayWithEvenIndices();
    }

    private static void sumNumberThatAreOdd() {
        int[] xs = {2, 1, 3, 4, 6, 8, 10};

        int sum = 0;

        for (int x: xs) {
            if (x %   2 != 0) {
                sum += x;
            }
        }

        System.out.println(sum);
    }

    private static void sumNumbersFromArrayWithEvenIndices() {
        int[] xs = new int[10];
        for (int i = 0; i < xs.length; i++) {
            xs[i] = 1;
        }

        int sum = 0;

        for (int i = 0; i < xs.length; i++) {
            if (i % 2 == 0) {
                sum += xs[i];
            }
        }

        System.out.println(sum);
    }
}
