package com.company.booleans;

public class ShortCircuit {
    public static void main(String[] args) {

        boolean visa = hasVisa();
        boolean packedStuff = hasPackedStuff();
        boolean hasTickets = hasTickets();

        boolean isReadyForVacation = visa && packedStuff && hasTickets;

        if (isReadyForVacation) {
            System.out.println("Ready for vacation");
        } else {
            System.out.println("Other time");
        }

        //printBooleanOperatorsResults();
//        bothTrue(true, false);
//        bothTrue(true, true);
    }

    private static boolean hasTickets() {
        System.out.println("Booking tickets");
        return true;
    }

    private static boolean hasVisa() {
        System.out.println("Checking for visa");
        return !true;
    }

    private static boolean hasPackedStuff() {
        System.out.println("Packing stuff");
        return true;
    }


    private static void printBooleanOperatorsResults() {
        for (boolean first : new boolean[]{true, false}) {
            for (boolean second : new boolean[]{true, false}) {
                System.out.println("first is " + first + ", second is " + second);
                System.out.println("first && second = " + (first && second));
                System.out.println("first || second = " + (first || second));
                System.out.println("!first = " + !first);
                System.out.println("!second = " + !second);
                System.out.println();
            }
        }
    }

    public static void bothTrue(boolean first, boolean second) {
        // if first is true and the second is true
        if (first && second) {
            // Print both true if both are true
            System.out.println("Both are true");
        }

        if (!first) {
            System.out.println("First is false");
        }

        if (!second) {
            System.out.println("Second is false");
        }
    }
}
